class Employee {
  constructor(name, age, salary) {
    this.name = name;
    this.age = age;
    this.salary = salary;
  }
// get/set for Employee class
  get getName() {
    return this.name;
  }

  set setName(name) {
    this.name = name;
  }

  get getAge() {
    return this.age;
  }

  set setAge(age) {
    this.age = age;
  }

  get getSalary() {
    return this.salary;
  }

  set setSalary(salary) {
    this.salary = salary;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this.lang = lang;
  }

  // new get for  Salary class
  get getSalary() {
    return this.salary * 3;
  }
}

let programmer1 = new Programmer("Li Doe", 30, 5000, ["JavaScript", "Python"]);
let programmer2 = new Programmer("Jane Smith", 35, 6000, ["Java", "C++"]);
let programmer3 = new Programmer("Alex Rich", 28, 8000, ["Swift", "React"]);
let programmer4 = new Programmer("Bob Dylan", 40, 3500, ["Pyton", "React"]);

console.log(programmer1);
console.log(programmer2);
console.log(programmer3);
console.log(programmer4);